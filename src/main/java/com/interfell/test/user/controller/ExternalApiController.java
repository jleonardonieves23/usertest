package com.interfell.test.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.interfell.test.user.domain.model.ExternalApiDto;
import com.interfell.test.user.service.IExternalApiService;

@RestController
@RequestMapping("/api")
public class ExternalApiController {

	@Autowired
	private IExternalApiService externalApiService;

	@PostMapping("/external")
	public ResponseEntity<ExternalApiDto> externalCall(@RequestParam String param) throws Exception {

		ExternalApiDto externalApiDto = externalApiService.externalApiCall(param);

		return new ResponseEntity<>(externalApiDto, HttpStatus.OK);
	}

}
