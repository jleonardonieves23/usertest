package com.interfell.test.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.interfell.test.user.domain.model.UserDto;
import com.interfell.test.user.service.IUserService;

@RestController
@RequestMapping("user")
public class UserController {

	@Autowired
	private IUserService userService;

	@GetMapping("/all")
	public ResponseEntity<List<UserDto>> getAllUser() throws Exception {
		
		List<UserDto> users = userService.listAllUser();

		return new ResponseEntity<>(users, HttpStatus.OK);
	}

	@GetMapping("/{email}")
	public ResponseEntity<UserDto> getByUserEmail(String email) throws Exception {
		
		UserDto user = userService.getUserEmail(email);

		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@PostMapping("/new")
	public ResponseEntity<UserDto> saveUser(@RequestBody UserDto userDto) throws Exception {
		
		UserDto useDto = userService.addNewUser(userDto);
		
		return new ResponseEntity<>(useDto, HttpStatus.OK);
	}


	@DeleteMapping("/{id}") 
	public ResponseEntity<String> deleteUser(Long id) throws Exception {
		
		userService.deleteUser(id);

		return new ResponseEntity<>("El usario fue eliminado conrrectamente",HttpStatus.OK);

	}

}
