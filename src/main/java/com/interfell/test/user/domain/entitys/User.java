package com.interfell.test.user.domain.entitys;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "usuario")
@Getter
@Setter
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_suario")
	private Long idUser;

	@Column(name = "nombre")
	private String name;

	@Column(name = "nombre_suario")
	private String userName;

	@Column(name = "correo", unique = true)
	private String email;

	@Column(name = "telefono", unique = true)
	private String phone;

}
