package com.interfell.test.user.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExternalApiDto {
	
	private Integer responseCode;
	
	private String descripcion;
	
	private Integer elapsedTime;
	
	private ResultDto result;
	
	private String mensaje;

}
