package com.interfell.test.user.domain.model;

import lombok.Data;

@Data
public class ResultDto {
	
	private String registerCount;
}
