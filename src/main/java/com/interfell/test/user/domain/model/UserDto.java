package com.interfell.test.user.domain.model;

import lombok.Data;

@Data
public class UserDto {
	
	private Long idUser;

	private String name;

	private String userName;

	private String email;

	private String phone;
	
}
