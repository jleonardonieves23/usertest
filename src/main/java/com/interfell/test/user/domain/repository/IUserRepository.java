package com.interfell.test.user.domain.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.interfell.test.user.domain.entitys.User;

public interface IUserRepository extends CrudRepository<User, Long> {
	
	Optional<User> findByEmail(String userName);

}
