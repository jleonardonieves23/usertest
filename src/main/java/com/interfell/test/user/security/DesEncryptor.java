package com.interfell.test.user.security;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class DesEncryptor {

    private static final String KEY = "ionix123"; 

    public static String encrypt(String data) throws Exception {
    	SecretKeySpec secretKeySpec = new SecretKeySpec(KEY.getBytes(), "DES");
		Cipher cipher = Cipher.getInstance("DES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		byte[] encryptedText = cipher.doFinal(data.getBytes("UTF8"));
		
		StringBuilder sb = new StringBuilder();
		for (byte b : encryptedText) {
			sb.append(String.format("%02X", b));
		}
		
		return sb.toString();
    }
}
