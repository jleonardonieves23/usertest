package com.interfell.test.user.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.interfell.test.user.domain.model.ExternalApiDto;
import com.interfell.test.user.security.DesEncryptor;

@Service
public class ExternalApiServiceImpl implements IExternalApiService {

	private static String URL = "https://my.api.mockaroo.com/test-tecnico/search/";

	@Override
	public ExternalApiDto externalApiCall(String param) throws Exception {

		String encryptedParam = DesEncryptor.encrypt(param.trim());

		Map<String, String> headers = new HashMap<>();
		headers.put("X-API-Key", "f2f719e0");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);

		HttpEntity<String> entity = new HttpEntity<>(httpHeaders);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ExternalApiDto> response = restTemplate.exchange(URL + encryptedParam, HttpMethod.GET, entity,
				ExternalApiDto.class);

		return response.getBody();
	}

}
