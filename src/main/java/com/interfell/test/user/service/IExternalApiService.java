package com.interfell.test.user.service;

import com.interfell.test.user.domain.model.ExternalApiDto;

public interface IExternalApiService {
	
	ExternalApiDto externalApiCall(String param) throws Exception;

}
