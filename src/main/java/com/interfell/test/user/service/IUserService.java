package com.interfell.test.user.service;

import java.util.List;

import com.interfell.test.user.domain.model.UserDto;

public interface IUserService {
	
	List<UserDto> listAllUser() throws Exception;
	
	UserDto getUserEmail(String email) throws Exception;
	
	UserDto addNewUser(UserDto user) throws Exception;
	
	void deleteUser(Long id) throws Exception;

}
