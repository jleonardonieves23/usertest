package com.interfell.test.user.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.interfell.test.user.domain.entitys.User;
import com.interfell.test.user.domain.model.UserDto;
import com.interfell.test.user.domain.repository.IUserRepository;
import com.interfell.test.user.service.mapper.UserDtoMapper;

@Service
public class UserImplService implements IUserService {
	
	private IUserRepository userRepository;

	public UserImplService(IUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public List<UserDto> listAllUser() throws Exception {
		List<User> user = (List<User>) userRepository.findAll(); 
		
		return user.stream().map(UserDtoMapper::userToUserDto).toList();
	}

	@Override
	public UserDto getUserEmail(String email) throws Exception {
		
		Optional<User> user = userRepository.findByEmail(email);
		
		if (user.isEmpty()) {
			throw new Exception(String.format("No existe usuario registrado con el correo %s", email));
		}

		return UserDtoMapper.userToUserDto(user.get());
	}

	@Override
	public UserDto addNewUser(UserDto user) throws Exception {
		User newUser = userRepository.save(UserDtoMapper.userDtoToEntity(user));		
		
		return UserDtoMapper.userToUserDto(newUser);
		
	}

	@Override
	public void deleteUser(Long id) throws Exception {
		
		Optional<User> user = getByIdUser(id);
		
		userRepository.delete(user.get());
		
		
	}
	
	private Optional<User> getByIdUser(Long id) throws Exception {
		Optional<User> user = userRepository.findById(id);
		
		if(user.isEmpty()) {
			throw new Exception(String.format("No existe usuario registrado con el id %s", id));
		}
		
		return  user;
	}

}
