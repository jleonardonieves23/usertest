package com.interfell.test.user.service.mapper;

import com.interfell.test.user.domain.entitys.User;
import com.interfell.test.user.domain.model.UserDto;

public class UserDtoMapper {

	private UserDtoMapper() {
		super();
	}
	
	public static UserDto userToUserDto(User entity) {
		UserDto dto = new UserDto();
		
		dto.setEmail(entity.getEmail());
		dto.setIdUser(entity.getIdUser());
		dto.setName(entity.getName());
		dto.setPhone(entity.getPhone());
		dto.setUserName(entity.getUserName());
		
		return dto;
		
	}
	
	public static User userDtoToEntity(UserDto dto) {
		User entity = new User();
		
		entity.setEmail(dto.getEmail());
		entity.setIdUser(dto.getIdUser());
		entity.setName(dto.getName());
		entity.setPhone(dto.getPhone());
		entity.setUserName(dto.getUserName());
		
		return entity;
		
	}
	

}
